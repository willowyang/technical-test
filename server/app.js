const loopback = require('loopback');
const boot = require('loopback-boot');
const app = (module.exports = loopback());
const changeCase = require('change-case');

const {dataSource} = app;

app.dataSource = function(name, config) {
  const ds = dataSource.call(this, name, config);
  const {naming} = config;
  const namingStrategies = {
    untransformed: (val) => val,
    snakecase: (val) => changeCase.snakeCase(val),
    camelcase: (val) => changeCase.camelCase(val),
    lowercase: (val) => val.toLowerCase(),
    uppercase: (val) => val.toUpperCase(),
    default: null,
  };
  const dbName =
    typeof naming === 'function' ? naming : namingStrategies[naming];
  if (dbName) {
    ds.connector.dbName = dbName;
  }
  return ds;
};

app.start = function(done) {
  // Bootstrap the application, configure models, datasources and middleware.
  // Sub-apps like REST API are mounted via boot scripts.
  boot(app, __dirname, function(err) {
    if (err) throw err;
    // start the web server
    const server = app.listen((err) => {
      app.emit('started');
      if (done) done(err, server);
    });
  });
};

exports = app;
