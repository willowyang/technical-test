const TestUser1 = {
  username: 'user1',
  password: 'test123',
  email: 'user1@company1.com',
};
const TestUser2 = {
  username: 'user2',
  password: 'test123',
  email: 'user2@company1.com',
};
const TestUser3 = {
  username: 'willow',
  password: 'test123',
  email: 'email@willowyang.pro',
};
const TestCustomer1 = {
  name: 'customer A',
  accountId: 1,
};
const TestCustomer2 = {
  name: 'customer B',
  accountId: 1,
};
const TestCustomer3 = {
  name: 'customer C',
  accountId: 1,
};
const TestCustomer4 = {
  name: 'customer D',
  accountId: 3,
};
const TestAccount1 = {
  name: 'business account A',
  description: 'a test business account',
  ownerId: 1,
};
const TestAccount2 = {
  name: 'business account B',
  description: 'a test business account',
  ownerId: 2,
};
const TestAccount3 = {
  name: 'business account C',
  description: 'a test business account',
  ownerId: 3,
};
const TestAccount4 = {
  name: 'business account D',
  description: 'a test business account',
  ownerId: 1,
};
const createBuildInTables = (app) => {
  return new Promise((resolve, reject) => {
    const ds = app.dataSources.maindb;
    ds.automigrate(['ACL', 'AccessToken', 'RoleMapping', 'Role'], (err) => {
      if (err) reject(err);
      resolve(app);
    });
  });
};
const terminate = (app) => {
  const ds = app.dataSources.maindb;
  ds.disconnect();
  process.exit();
};
const createUserTable = (app) => {
  return new Promise((resolve, reject) => {
    const ds = app.dataSources.maindb;
    ds.automigrate('User', (err) => {
      if (err) reject(err);

      app.models.User.create([TestUser1, TestUser2, TestUser3], (err, User) => {
        if (err) reject(err);
        console.log('User model created: \n', User);
        resolve(app);
      });
    });
  });
};
const createCustomerTable = (app) => {
  return new Promise((resolve, reject) => {
    const ds = app.dataSources.maindb;
    ds.automigrate('Customer', (err) => {
      if (err) reject(err);
      app.models.Customer.create(
        [TestCustomer1, TestCustomer2, TestCustomer3, TestCustomer4],
        (err, Customer) => {
          if (err) reject(err);
          console.log('Customer model created: \n', Customer);
          resolve(app);
        }
      );
    });
  });
};
const createAccountTable = (app) => {
  return new Promise((resolve, reject) => {
    const ds = app.dataSources.maindb;
    ds.automigrate('Account', function(err) {
      if (err) reject(err);
      app.models.Account.create(
        [TestAccount1, TestAccount2, TestAccount3, TestAccount4],
        (err, Account) => {
          if (err) reject(err);
          console.log('Account model created: \n', Account);
          resolve(app);
        }
      );
    });
  });
};
module.exports = (app) => {
  createBuildInTables(app)
    .then(createUserTable)
    .then(createCustomerTable)
    .then(createAccountTable)
    .then(terminate);
};
