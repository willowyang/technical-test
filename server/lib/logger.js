module.exports = {
  error: (message, error) => {
    console.error(message, error);
  },
  info: (message) => {
    console.info(message);
  },
  debug: (message) => {
    console.debug(message);
  },
  critical: (message, error) => {
    console.error(message, error);
  },
};
