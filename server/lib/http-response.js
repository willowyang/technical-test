const HttpStatusCode = require('./http-status-code');
const Logger = require('./logger');

const errorResponse = (res, statusCode, message) => {
  res.status(statusCode).json({
    error: {
      message,
    },
  });
};

const successResponse = (res, statusCode, payload) => {
  if (payload) {
    res.status(statusCode).json(payload);
  }
  res.status(statusCode).send();
};

module.exports = {
  unprocessableEntityErrorResponse: (res, error, errorLogMessage) => {
    if (error && error.statusCode === HttpStatusCode.UnprocessableEntity) {
      return errorResponse(
        res,
        HttpStatusCode.UnprocessableEntity,
        error.message
      );
    }
    Logger.error(errorLogMessage, error);
    return errorResponse(
      res,
      HttpStatusCode.InternalServerError,
      'Internal server error'
    );
  },
  accessTokenErrorResponse: (res, error, errorLogMessage) => {
    if (
      error &&
      (error.statusCode === HttpStatusCode.Unauthorized ||
        error.code === 'LOGIN_FAILED')
    ) {
      return errorResponse(res, HttpStatusCode.Unauthorized, error.message);
    }
    Logger.error(errorLogMessage, error);
    return errorResponse(
      res,
      HttpStatusCode.InternalServerError,
      'Internal server error'
    );
  },
  unauthorizedErrorResponse: (res, message) => {
    errorResponse(res, HttpStatusCode.Unauthorized, message);
  },
  validateErrorResponse: (res, message) => {
    errorResponse(res, HttpStatusCode.UnprocessableEntity, message);
  },
  internalErrorResponse: (res) => {
    errorResponse(
      res,
      HttpStatusCode.InternalServerError,
      'Internal server error'
    );
  },
  okResponse: (res, payload = null) => {
    successResponse(res, HttpStatusCode.Ok, payload);
  },
  createdResponse: (res, payload) => {
    successResponse(res, HttpStatusCode.Created, payload);
  },
};
