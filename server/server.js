
const loopback = require('loopback');
const boot = require('loopback-boot');
const changeCase = require('change-case');

const app = (module.exports = loopback());
const {dataSource} = app;

app.dataSource = function(name, config) {
  const ds = dataSource.call(this, name, config);
  const {naming} = config;
  const namingStrategies = {
    untransformed: (val) => val,
    snakecase: (val) => changeCase.snakeCase(val),
    camelcase: (val) => changeCase.camelCase(val),
    lowercase: (val) => val.toLowerCase(),
    uppercase: (val) => val.toUpperCase(),
    default: null,
  };
  const dbName =
    typeof naming === 'function' ? naming : namingStrategies[naming];
  if (dbName) {
    ds.connector.dbName = dbName;
  }
  return ds;
};

app.start = function() {
  // start the web server
  return app.listen(function() {
    app.emit('started');
    var baseUrl = app.get('url').replace(/\/$/, '');
    console.log('Web server listening at: %s', baseUrl);
    // if (app.get('loopback-component-explorer')) {
    //   var explorerPath = app.get('loopback-component-explorer').mountPath;
    //   console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
    // }
  });
};

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function(err) {
  if (err) throw err;

  // start the server if `$ node server.js`
  if (require.main === module) app.start();
});
