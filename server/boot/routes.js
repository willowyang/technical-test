const HttpResponse = require('../lib/http-response');
const config = require('./config');

module.exports = function(app) {
  const User = app.models.User;
  const AccessToken = app.models.AccessToken;
  const Account = app.models.Account;
  const Customer = app.models.Customer;

  const validationLogin = (body, res) => {
    return new Promise((resolve, reject) => {
      if (!body || !body.email) {
        return HttpResponse.unauthorizedErrorResponse(
          res,
          'Email address is required'
        );
      }
      if (!body.password) {
        return HttpResponse.unauthorizedErrorResponse(
          res,
          'Password is required'
        );
      }
      resolve();
    });
  };

  const validateAccessToken = (query, res) => {
    return new Promise((resolve, reject) => {
      if (!query || !query.access_token) {
        // return 401:unauthorized if access_token is not present
        return HttpResponse.unauthorizedErrorResponse(
          res,
          'a valid access_token is required'
        );
      }
      // have to use call back here due to Loopback build in function is incorrectly implemented
      AccessToken.resolve(query.access_token, (err, token) => {
        if (err || !token) {
          return HttpResponse.unauthorizedErrorResponse(
            res,
            'a valid access_token is required'
          );
        }
        resolve(token);
      });
    });
  };

  app.get('/test', (req, res) => {
    res.send('ok');
  });
  app.post('/api/auth/login', (req, res) => {
    validationLogin(req.body, res)
      .then(() => {
        // use build in User model to handle user login
        const {email, password} = req.body;
        return User.login(
          {
            email: email,
            password: password,
            ttl: config.accessToken.ttl,
          },
          'user'
        );
      })
      .then((token) => {
        HttpResponse.okResponse(res, {
          accessToken: token.id,
        });
      })
      .catch((err) => {
        HttpResponse.accessTokenErrorResponse(
          res,
          err,
          'Error happened on user login!'
        );
      });
  });

  app.post('/api/auth/logout', (req, res) => {
    validateAccessToken(req.query, res)
      .then(() => {
        return User.logout(req.query.access_token);
      })
      .then(() => {
        HttpResponse.okResponse(res, {status: 'logged out'});
      })
      .catch((err) => {
        HttpResponse.accessTokenErrorResponse(
          res,
          err,
          'Error happened on user logout!'
        );
      });
  });

  app.get('/api/accounts', (req, res) => {
    validateAccessToken(req.query, res)
      .then(() => {
        return Account.find({include: ['customers', 'owner']}).then(
          (accounts) => {
            return HttpResponse.okResponse(res, {
              accounts,
            });
          }
        );
      })
      .catch((err) => {
        HttpResponse.accessTokenErrorResponse(
          res,
          err,
          'Error happened on listing all accounts'
        );
      });
  });

  app.get('/api/customers', (req, res) => {
    validateAccessToken(req.query, res)
      .then(() => {
        return Customer.find({include: 'account'}).then((customers) => {
          return HttpResponse.okResponse(res, {
            customers,
          });
        });
      })
      .catch((err) => {
        HttpResponse.accessTokenErrorResponse(
          res,
          err,
          'Error happened on listing all customers'
        );
      });
  });

  app.post('/api/customers', (req, res) => {
    validateAccessToken(req.query, res)
      .then(() => {
        return Customer.create(req.body).then((customer) => {
          return HttpResponse.okResponse(res, {
            customer,
          });
        });
      })
      .catch((err) => {
        HttpResponse.unprocessableEntityErrorResponse(
          res,
          err,
          'Error happened on creating a new customer'
        );
      });
  });

  app.put('/api/customers', (req, res) => {
    validateAccessToken(req.query, res)
      .then(() => {
        return Customer.upsert(req.body).then((customer) => {
          return HttpResponse.okResponse(res, {
            customer,
          });
        });
      })
      .catch((err) => {
        HttpResponse.unprocessableEntityErrorResponse(
          res,
          err,
          'Error happened on updating a customer'
        );
      });
  });

  app.delete('/api/customers/:id', (req, res) => {
    validateAccessToken(req.query, res)
      .then(() => {
        if (!req.params || !req.params || !req.params.id) {
          return HttpResponse.validateErrorResponse(
            res,
            'variable id is missing in the path'
          );
        }
        return Customer.destroyById(req.params.id).then((customer) => {
          return HttpResponse.okResponse(res, {
            customer,
          });
        });
      })
      .catch((err) => {
        HttpResponse.unprocessableEntityErrorResponse(
          res,
          err,
          'Error happened on deleting a customer'
        );
      });
  });
};
