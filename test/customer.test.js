const ValidUser = {email: 'email@willowyang.pro', password: 'test123'};
const ValidCustomer = {name: 'Test Customer', accountId: 1};
const UpsertCustomer = {name: 'Test Upsert Customer', accountId: 2};

describe('Test customer endpoints', () => {
  const app = require('../server/server');
  const request = require('supertest')(app);

  test('create customer should fail without a valid access_token', (done) => {
    request.post('/api/customers').then((response) => {
      expect(response.statusCode).toBe(401);
      expect(response.text).toBe(
        '{"error":{"message":"a valid access_token is required"}}'
      );
      done();
    });
  });
  test('create customer should fail with empty payload', (done) => {
    request
      .post('/api/auth/login')
      .send(ValidUser)
      .then((response) => {
        const {accessToken} = JSON.parse(response.text);
        request
          .post('/api/customers?access_token=' + accessToken)
          .then((response) => {
            expect(response.statusCode).toBe(422);
            expect(response.text).toBe(
              '{"error":{"message":"The `Customer` instance is not valid. Details: `name` can\'t be blank (value: undefined); `accountId` can\'t be blank (value: undefined)."}}'
            );
            done();
          });
      });
  });
  test('create customer should be success with correct data', (done) => {
    request
      .post('/api/auth/login')
      .send(ValidUser)
      .then((response) => {
        const {accessToken} = JSON.parse(response.text);
        request
          .post('/api/customers?access_token=' + accessToken)
          .send(ValidCustomer)
          .then((response) => {
            expect(response.statusCode).toBe(200);
            const {customer} = JSON.parse(response.text);
            expect(customer).toHaveProperty('id');
            expect(customer).toHaveProperty('name', ValidCustomer.name);
            expect(customer).toHaveProperty(
              'accountId',
              ValidCustomer.accountId
            );
            done();
          });
      });
  });
  test('delete customer should fail without a valid access_token', (done) => {
    request.delete('/api/customers/50').then((response) => {
      expect(response.statusCode).toBe(401);
      expect(response.text).toBe(
        '{"error":{"message":"a valid access_token is required"}}'
      );
      done();
    });
  });
  test('delete customer should success without a valid customer id', (done) => {
    request
      .post('/api/auth/login')
      .send(ValidUser)
      .then((response) => {
        const {accessToken} = JSON.parse(response.text);
        request
          .post('/api/customers?access_token=' + accessToken)
          .send(ValidCustomer)
          .then((response) => {
            expect(response.statusCode).toBe(200);
            const {customer} = JSON.parse(response.text);
            request
              .delete(
                '/api/customers/' + customer.id + '?access_token=' + accessToken
              )
              .then((response) => {
                expect(response.statusCode).toBe(200);
                expect(response.text).toBe('{"customer":{"count":1}}');
                done();
              });
          });
      });
  });
  test('upsert customer should fail without a valid access_token', (done) => {
    request.put('/api/customers').then((response) => {
      expect(response.statusCode).toBe(401);
      expect(response.text).toBe(
        '{"error":{"message":"a valid access_token is required"}}'
      );
      done();
    });
  });
  test('upsert customer should fail with empty payload', (done) => {
    request
      .post('/api/auth/login')
      .send(ValidUser)
      .then((response) => {
        const {accessToken} = JSON.parse(response.text);
        request
          .put('/api/customers?access_token=' + accessToken)
          .then((response) => {
            expect(response.statusCode).toBe(422);
            expect(response.text).toBe(
              '{"error":{"message":"The `Customer` instance is not valid. Details: `name` can\'t be blank (value: undefined); `accountId` can\'t be blank (value: undefined)."}}'
            );
            done();
          });
      });
  });
  test('upsert customer should success without a valid customer id', (done) => {
    request
      .post('/api/auth/login')
      .send(ValidUser)
      .then((response) => {
        const {accessToken} = JSON.parse(response.text);
        request
          .post('/api/customers?access_token=' + accessToken)
          .send(ValidCustomer)
          .then((response) => {
            expect(response.statusCode).toBe(200);
            const {customer} = JSON.parse(response.text);
            request
              .put('/api/customers?access_token=' + accessToken)
              .send({id: customer.id, ...UpsertCustomer})
              .then((response) => {
                const {customer} = JSON.parse(response.text);
                expect(response.statusCode).toBe(200);
                expect(customer).toHaveProperty('id');
                expect(customer).toHaveProperty('name', UpsertCustomer.name);
                expect(customer).toHaveProperty(
                  'accountId',
                  UpsertCustomer.accountId
                );
                done();
              });
          });
      });
  });
});
