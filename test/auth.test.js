const ValidUser = {email: 'email@willowyang.pro', password: 'test123'};
const UserWithoutEmail = {password: 'test123'};
const UserWithoutPassword = {email: 'email@willowyang.pro'};
const UserWithInvalidPassword = {
  email: 'email@willowyang.pro',
  password: 'test123456',
};
const UserWithInvalidEmail = {
  email: 'email1234@willowyang.pro',
  password: 'test123',
};

describe('Test authendication endpoints', () => {
  const app = require('../server/server');
  const request = require('supertest')(app);

  test('login should fail with empty payload', (done) => {
    request.post('/api/auth/login').then((response) => {
      expect(response.statusCode).toBe(401);
      expect(response.text).toBe(
        '{"error":{"message":"Email address is required"}}'
      );
      done();
    });
  });
  test('login should fail without a email in the request', (done) => {
    request
      .post('/api/auth/login')
      .send(UserWithoutEmail)
      .then((response) => {
        expect(response.statusCode).toBe(401);
        expect(response.text).toBe(
          '{"error":{"message":"Email address is required"}}'
        );
        done();
      });
  });
  test('login should fail without a password in the request', (done) => {
    request
      .post('/api/auth/login')
      .send(UserWithoutPassword)
      .then((response) => {
        expect(response.statusCode).toBe(401);
        expect(response.text).toBe(
          '{"error":{"message":"Password is required"}}'
        );
        done();
      });
  });
  test('login should fail with incorrect username', (done) => {
    request
      .post('/api/auth/login')
      .send(UserWithInvalidEmail)
      .then((response) => {
        expect(response.statusCode).toBe(401);
        expect(response.text).toBe('{"error":{"message":"login failed"}}');
        done();
      });
  });
  test('login should fail with incorrect password', (done) => {
    request
      .post('/api/auth/login')
      .send(UserWithInvalidPassword)
      .then((response) => {
        expect(response.statusCode).toBe(401);
        expect(response.text).toBe('{"error":{"message":"login failed"}}');
        done();
      });
  });
  test('user should be able to login with correct username and password', (done) => {
    request
      .post('/api/auth/login')
      .send(ValidUser)
      .then((response) => {
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text)).toHaveProperty('accessToken');
        done();
      });
  });
  test('user should be able to logout with correct accessToken', (done) => {
    request
      .post('/api/auth/login')
      .send(ValidUser)
      .then((response) => {
        const {accessToken} = JSON.parse(response.text);
        request
          .post('/api/auth/logout?access_token=' + accessToken)
          .then((response) => {
            expect(response.statusCode).toBe(200);
            done();
          });
      });
  });
  test('logout request should fail without a accessToken', (done) => {
    request.post('/api/auth/logout').then((response) => {
      expect(response.statusCode).toBe(401);
      expect(response.text).toBe(
        '{"error":{"message":"a valid access_token is required"}}'
      );
      done();
    });
  });
  test('logout request should fail with a invalid accessToken', (done) => {
    request.post('/api/auth/logout?access_token=123').then((response) => {
      expect(response.statusCode).toBe(401);
      expect(response.text).toBe(
        '{"error":{"message":"a valid access_token is required"}}'
      );
      done();
    });
  });
});
