# Technical Test Solution

The project is a solution to a Technical Test.

## Summary

Loopback framework has been chosen for API server development [more in Loopback](#loopback).

All endpoints are securely protected by OAuth 2.0 Access Token [more in Authentication](#authentication).

This project has a low amount of integration test coverage. Due to time limitation. [more in integration test](#test)

A persistent storage has been built using MySQL db that lives in a docker container. [more in minikube orchestration](#minikube-orchestration)

## Environment

the following applications must be installed

`node >= 8`
`minikube >= v0.28.0`
`docker >= 18.06.0-ce`
`kubectl >= v1.11.0`

run the following command to install project dependencies

`yarn install`

## Start the server

Before the API server can be run, the MySQL db container needs to be running. To do so, we need to create a persistent volume first. Run the following,

`yarn kube-create-pv`

and to deploy the MySQL container

`yarn kube-create-mysql`

One more command before starting the server, that is the loopback migration script.

`yarn init-db`

start the project:

`yarn start`

### Other commands

integration test:

`yarn test`

linting:

`yarn lint`

## Loopback

[Loopback](https://loopback.io/) is a node framework that helps creating REST API server quickly.

Routes are defined in `server/boot/routes.js`. Some support functions are created in the `server/lib` for response and log handling.

The accounting model and the customer model are created for this project. Data are stored in the account table and customer table. These 2 models are configured with relations of each other so listing the account will also display the associated customers.

User accounts are stored in the build-in user table. Other db tables such as the access_token table are build-in tables that are required for loopback server to run. They are included in the migration scripts.

## Authentication

A valid OAuth 2.0 Access Token must be provided in order to access any endpoint (except the login endpoint).

To obtain an access token, call the login endpoint with valid email and password. Each access token has an expiration time of 8 hours (expiration time can be configured in `server/boot/config.json`).

All user passwords are encrypted by using bcrypt.

## Test

Jest has been chosen as the test framework for this project.

Integration tests are located in the `test` directory. These tests covered different scenarios with both positive and negative inputs.

Due to time limitation, integration tests are only implemented for the authentication endpoints and customer endpoints. Unit tests are not implemented in this project.

## Minikube orchestration

For persistent data storage, runs a mysql db in minikube.

Kube deployment files are located in the `docker` directory. There are 2 files one for setting up the persistent volume and another file for setting up the mysql server.

Node port `30306` is exposed in local dev environment so that the db can be accessible from the app.

- Run the following for detail info about the pod we just created

`kubectl describe pods mysql`

- Run the following to display the mysql log

`kubectl --v=8 logs mysql`

- To delete and clean up, run

`yarn kube-delete-mysql` and `yarn kube-delete-pv`

## Database migration/initilisaction

This project demonstrated 2 ways to initialise a db.

- 1. Do it in kube. Create a ConfigMap and mount it to the `/docker-entrypoint-initdb.d`

- 2. Do it in code. Create a migration script in `server/migration/create-models.js`. Execute it as follow

`yarn init-db`

Db config in the code is located in `server/datasources.json`.

## API reference

The API server is running on localhost and listening to port 8000 in the dev environment.

| Method | Endpoint           | Sample Commends                                                                                                                                         |
| ------ | ------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------- |
| POST   | /api/auth/login    | `curl -X POST http://localhost:8000/api/auth/login -H 'Content-Type: application/json' -d '{"email": "email@willowyang.pro","password": "test123"}'`    |
| POST   | /api/auth/logout   | `curl -X POST http://localhost:8000/api/auth/logout?access_token=[token]`                                                                               |
| GET    | /api/accounts      | `curl -X GET http://localhost:8000/api/accounts?access_token=[token]`                                                                                   |
| GET    | /api/customers     | `curl -X GET http://localhost:8000/api/customers?access_token=[token]`                                                                                  |
| POST   | /api/customers     | `curl -X POST http://localhost:8000/api/customers?access_token=[token] -H 'Content-Type: application/json' -d '{"name": "new cust","accountId": 1}'`    |
| PUT    | /api/customers     | `curl -X PUT http://localhost:8000/api/customers?access_token=[token] -H 'Content-Type: application/json' -d '{"name": "updated cust","accountId": 3}'` |
| DELETE | /api/customers/:id | `curl -X DELETE http://localhost:8000/api/customers/[id]?access_token=[token]`                                                                          |

## Programming style

Javascript ES6 is used in this project. The code is written in functional programming pattern. Loopback 3 doesn't support typescript out of the box. Due to time limitation, not able to convert it to typescript. This can be an improvement in the future.

Some of the build-in code in Loopback is very out of date. For example, it is still using callbacks. They are using some of the outdated libaraies such as bluebird and async. Most of them can be replaced with ES6.
